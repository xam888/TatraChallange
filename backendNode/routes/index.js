var express = require('express');
var router = express.Router();
var db = require('../queries');
var cors = require('cors')
var whitelist = ['http://10.0.2.2:8081', 'null', undefined, 'http://localhost:8081']



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

router.get('/api/vector-tiles/:layername/:z/:x/:y.pbf', cors(corsOptions), db.getVectorTiles);

router.get('/api/hikingPaths', db.getHikingPaths);
router.get('/api/peaks', db.getPeaks);
router.get('/api/users/signin/:login/:password', db.signIn);
router.post('/api/users/signup/:login/:password/:email', db.signUp);

module.exports = router;







