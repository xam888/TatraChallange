// var mapnik = require('mapnik')
// mapnik.register_default_input_plugins()
var promise = require('bluebird');
var SphericalMercator = require('sphericalmercator')
var zlib = require('zlib');


var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://postgres:tatra@35.187.126.214:5432/tatra';
var db = pgp(connectionString);

// add query functions

module.exports = {
  getHikingPaths: getHikingPaths,
  getPeaks: getPeaks,
  getVectorTiles: getVectorTiles,
  signIn: signIn,
  signUp: signUp
};

function signIn(req, res, next) {
  var login = req.params.login;
  var password = req.params.password
  db.one("select * from users where login = '" + login + "' and password = '" + password + "'")
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data
        });
    })
    .catch(function (err) {
      res.status(404).json({
        status: 'failure',
        data: {}
      });
    });
}


function getVectorTiles(req, res, next) {
  console.info(req.originalUrl)
  var mercator = new SphericalMercator({
    size: 256 //tile size
  })
  var bbox = mercator.bbox(
    +req.params.x,
    +req.params.y,
    +req.params.z,
    false,
    '4326'
  )
  db.any('select st_asgeojson(st_transform(hiking_paths.geometry, 4326)) as geometry from "hiking_paths" where st_intersects(st_transform(geometry, 4326), ST_MakeEnvelope( ' + bbox[0] + ', ' + bbox[1] + ', ' + bbox[2] + ', ' + bbox[3] + ', 4326)::geometry)')
    .then(function (geojson) {
      var features = [];
      geojson.forEach(function (path) {
        path.geometry = JSON.parse(path.geometry)
        features.push({
          type: "Feature",
          geometry: path.geometry
        })
      })

      var geojson = {
        type: "FeatureCollection",
        features: features
      }
      // var vtile = new mapnik.VectorTile(+req.params.z, +req.params.x, +req.params.y)
      // vtile.addGeoJSON(JSON.stringify(geojson), 'layername')
      // res.setHeader('Content-Encoding', 'deflate')
      // res.setHeader('Content-Type', 'application/x-protobuf')
      // zlib.deflate(vtile.getData(), function (err, pbf) {
      //   console.info(pbf);
      //   var base64 = pbf.toString('base64');
      //   res.send(base64);
      // })
    })
    .catch(function (err) {
      res.status(404).json({
        status: 'failure',
        data: {}
      });
    });
}

function getHikingPaths(req, res, next) {
  db.any('select * from "hiknig_paths_columns_json"')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function getPeaks(req, res, next) {
  db.any('select * from "peaks"')
    .then(function (data) {
      var features = [];      
      data.forEach(function (peak) {
        peak.geometry = JSON.parse(peak.geometry)
        features.push({
          type: "Feature",
          geometry: peak.geometry,
          properties: {
            name: peak.name,
            elevation: peak.ele
          }
        })
      })
      var geojson = {
        type: "FeatureCollection",
        features: features
      }
      var base64 = new Buffer(JSON.stringify(geojson)).toString('base64');
      // var base64 = geojson.toString('base64');
      res.send(base64);
    })
    .catch(function (err) {
      return next(err);
    });
}

function signUp(req, res, next) {
  var login = req.params.login;
  var password = req.params.password;
  var email = req.params.email;
  db.one("select * from users where login = '" + login + "' or email = '" + email + "'")
    .then(function (data) {
      res.status(404)
        .json({
          status: 'failure',
          message: 'login or mail is used by other user',
          data: data
        });
    })
    .catch(function (err) {
      db.none('insert into users(login, password, email)' +
        "values ('" + login + "', '" + password + "', '" + email + "')")
        .then(function () {
          res.status(200)
            .json({
              status: 'success',
              message: 'Inserted one user'
            });
        })
        .catch(function (err) {
          return next(err);
        });
    });
}

