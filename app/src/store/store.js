import { createStore} from 'redux';

const reducer = (state, action) => {
  switch (action.type) {
    case 'SETDATA':
      return { ...state, counter: state.counter + 1 };
    default:
      return state;
    }
};

export default function configureStore(initialState) {
	return createStore(
		reducer,
		{ peaks: {}}
	);
}