import React, { Component } from 'react';
import {
  View, Text, WebView, StyleSheet, ToolbarAndroid, ProgressBarAndroid
} from 'react-native';
import WebViewBridge from 'react-native-webview-bridge';
import Spinner from 'react-native-loading-spinner-overlay';
import { Navigation } from 'react-native-navigation';

export default class Map extends Component {

  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.state = {
      spinnerVisible: false,
      appMode: appMode.dev
    };
  }

  static navigatorButtons = {
    rightButtons: [
      {
        icon: require('../img/magnifier.png'),
        id: 'route'
      },
      {
        icon: require('../img/location.png'),
        id: 'location'
      },
    ]
  };

  onNavigatorEvent(event) {
    if (event.id === 'route') {
      this.props.navigator.showInAppNotification({
        screen: "Routing",
        autoDismiss: false
      });
      // this.onToggleTabsPress();
    }
    if (event.id === "location") {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          var initialPosition = JSON.stringify(position);
          this.setState({ initialPosition });
          this.refs.webviewbridge.sendToBridge(JSON.stringify(position))
        },
        (error) => alert(JSON.stringify(error)),
        { enableHighAccuracy: true, timeout: 5000, maximumAge: 1000 }
      );
    }
    if (event.id === "willDisappear") {
      this.props.navigator.dismissInAppNotification();
    }
  }

  onToggleTabsPress() {
    this.props.navigator.toggleTabs({
      to: this.tabsHidden ? 'shown' : 'hidden'
    });
    this.tabsHidden = !this.tabsHidden;
  }

  componentDidMount() {
    // navigator.geolocation.getCurrentPosition(
    //   (position) => {
    //     var initialPosition = JSON.stringify(position);
    //     this.refs.webviewbridge.sendToBridge(JSON.stringify(position))
    //     this.setState({initialPosition});
    //   },
    //   (error) => alert(JSON.stringify(error)),
    //   {enableHighAccuracy: true, timeout: 5000, maximumAge: 1000}
    // );
    navigator.geolocation.watchPosition((position) => {
      var lastPosition = JSON.stringify(position);
      this.setState({ lastPosition });
      this.refs.webviewbridge.sendToBridge(JSON.stringify(position))
    });
  }


  onBridgeMessage(message) {
    switch (message) {
      case "componentLoaded":
        this.setState({ spinnerVisible: false })
      default:
        null
    }
  }

  static navigationOptions = {
    title: 'Map',
  };
  render() {
    return (

      <View style={styles.container} >
        <Spinner visible={this.state.spinnerVisible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
        <WebViewBridge pointerEvents={'none'}
          ref="webviewbridge"
          source={this.state.appMode}
          scrollEnabled={true}
          scalesPageToFit={true}
          javaScriptEnabled={true}
          startInLoadingState={true}
          onBridgeMessage={this.onBridgeMessage.bind(this)}
        />
      </View>
    );
  }
}

const appMode = {
  dev: require('./Map/map.html'),
  dist: { uri: 'file:///android_asset/map.html' }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

