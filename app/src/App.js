import { Platform } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './store/store'
import { Navigation } from 'react-native-navigation';
import { registerScreens } from './screens';

const store = configureStore();
registerScreens(store, Provider);


const SHOW_SHARED_ELEMENT_SCREENS = false;

const createTabs = () => {
  const sharedElementScreens = Platform.OS === 'android' ? [
    {
      label: 'Card',
      screen: 'example.CardScreen',
      icon: require('../img/list.png'),
      title: 'Shared Element Transition'
    },
    {
      label: 'SET',
      screen: 'example.ListScreen',
      icon: require('../img/list.png'),
      title: 'Shared Element Transition'
    }
  ] : null;

  let tabs = [
    {
      label: 'One',
      screen: 'example.FirstTabScreen',
      icon: require('../img/one.png'),
      selectedIcon: require('../img/one_selected.png'),
      title: 'Screen One'
    },
    {
      label: 'Home',
      screen: 'Home',
      icon: require('../img/two.png'),
      selectedIcon: require('../img/two_selected.png'),
      title: 'Signing',
      navigatorStyle: {
        tabBarBackgroundColor: '#4dbce9',
      }
    },
    {
      label: 'Map',
      screen: 'Map',
      icon: require('../img/two.png'),
      selectedIcon: require('../img/two_selected.png'),
      title: 'Map',
      navigatorStyle: {
        tabBarBackgroundColor: '#4dbce9',
      }
    },
    {
      label: 'Two',
      screen: 'example.SecondTabScreen',
      icon: require('../img/two.png'),
      selectedIcon: require('../img/two_selected.png'),
      title: 'Screen Two',
      navigatorStyle: {
        tabBarBackgroundColor: '#4dbce9',
      }
    }
  ];
  // if (Platform.OS === 'android') {
  //   tabs.push({
  //     label: 'Collapsing',
  //     screen: 'example.CollapsingTopBarScreen',
  //     icon: require('../img/one.png'),
  //     title: 'Collapsing',
  //   });
  //   if (SHOW_SHARED_ELEMENT_SCREENS) {
  //     tabs = [...sharedElementScreens, ...tabs];
  //   }
  // }
  return tabs;
};
// this will start our app
Navigation.startTabBasedApp({
  tabs: createTabs(),
  appStyle: {
    tabBarBackgroundColor: '#0f2362',
    tabBarButtonColor: '#ffffff',
    tabBarSelectedButtonColor: '#63d7cc',
    tabFontFamily: 'BioRhyme-Bold',
    forceTitlesDisplay: true
  },
  drawer: {
    left: {
      screen: 'example.SideMenu'
    }
  }
});
//Navigation.startSingleScreenApp({
//  screen: {
//    screen: 'example.FirstTabScreen',
//    title: 'Navigation',
//    navigatorStyle: {
//      navBarBackgroundColor: '#4dbce9',
//      navBarTextColor: '#ffff00',
//      navBarSubtitleTextColor: '#ff0000',
//      navBarButtonColor: '#ffffff',
//      statusBarTextColorScheme: 'light'
//    }
//  },
//  drawer: {
//    left: {
//      screen: 'example.SideMenu'
//    }
//  }
//});