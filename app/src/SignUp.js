import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  ActivityIndicator,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';


export default class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      email: '',
      spinnerVisible: false,
      signUpFailed: ''
    }
  }
  goToMap() {
    const { navigate } = this.props.navigation;
    this.setState({ spinnerVisible: false });
    navigate('Map')
  }

  singUp() {
    var url = 'http://192.168.1.28:3000/api/users/signup/' +
    this.state.login + '/' + this.state.password + '/' + this.state.email;
    this.setState({ spinnerVisible: true });
    this.setState({ signUpFailed: '' });
    fetch(url, {
      method: 'POST'
    })
      .then((response) => {
        debugger
         if(response.status === 200){
           this.goToMap();
           this.setState({ spinnerVisible: false });
         }
        else {
          this.setState({ signUpFailed: 'Signing failed...!' })
          this.setState({ spinnerVisible: false });
        }
        response.json()
      }
      )
      .catch((error) => {
        console.error(error);
      });
  }
  static navigationOptions = {
    title: 'SignUp',
  };
  render() {
    return (
      <View>
        <Spinner visible={this.state.spinnerVisible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
        <TextInput placeholder="login"
          onChangeText={(text) => {
            this.setState({ login: text })
          }}
        />
        <TextInput placeholder="password"
          onChangeText={(text) => {
            this.setState({ password: text })
          }}
        />
        <TextInput placeholder="repeat password"
          onChangeText={(text) => {
            this.setState({ password: text })
          }}
        />
        <TextInput placeholder="e-mail"
          onChangeText={(text) => {
            this.setState({ email: text })
          }}
        />
        <Button disabled={!this.state.login || !this.state.password || !this.state.email || this.state.spinnerVisible} title="Sign Up" onPress={this.singUp.bind(this)}>
        </Button>
        {/*<ActivityIndicator
          style={{
            height: 80,
            opacity: this.state.spinnerVisible ? 1 : 0
          }}
          size="large"
        />*/}
        <Text>{this.state.signUpFailed}</Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3b5998',
    height: '100%'
  },
  webView: {
    height: 350,
  },
});

