import Autocomplete from 'react-native-autocomplete-input';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Keyboard
} from 'react-native';

export default class RoutingAutocomplete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      peaks: ['Giewont', 'Rysy', 'Kasprowy Wierch'],
      query: ''
    };
  }

  findPeak(query) {
    if (query === '') {
      return [];
    }
    const { peaks } = this.state;
    const regex = new RegExp(`${query.trim()}`, 'i');
    return peaks.filter(peak => peak.search(regex) >= 0);
  }

  render() {
    const { query } = this.state;
    const peaks = this.findPeak(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();


    return (
      <View style={styles.container}>
        <Autocomplete
          autoCapitalize="none"
          autoCorrect={false}
          containerStyle={styles.autocompleteContainer}
          data={peaks.length === 1 && comp(query, peaks[0]) ? [] : peaks}
          defaultValue={query}
          onChangeText={text => this.setState({ query: text })}
          placeholder="Wyszukaj..."
          renderItem={(peaks) => (
            <TouchableOpacity onPress={() => {
              this.setState({ query: peaks })
              Keyboard.dismiss();
            }
            }>
              <Text style={styles.itemText}>
                {peaks}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '50%',
    margin: 30,
    flex: 1
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  itemText: {
    fontSize: 15,
    margin: 2
  }
});
