import React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Button,
  ActivityIndicator,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';


export default class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      spinnerVisible: false,
      loginFailed: ''
    }
  }
  goToMap() {
    const { navigate } = this.props.navigation;
    this.setState({ spinnerVisible: false });
    navigate('Map')
  }

  singIn() {
    var url = 'http://192.168.1.28:3000/api/users/signin/' + this.state.login + '/' + this.state.password;
    this.setState({ spinnerVisible: true });
    this.setState({ loginFailed: '' });
    fetch(url)
      .then((response) => {
        if(response.status === 200){
          this.goToMap();
        }
        else{
          this.setState({ spinnerVisible: false });
          this.setState({ loginFailed: 'Signing failed...!' })
        }
      })
      .catch((error) => {
        this.setState({ spinnerVisible: false });
        console.error(error);
      });
  }
  static navigationOptions = {
    title: 'SignIn',
  };
  render() {
    return (
      <View>
        <Spinner visible={this.state.spinnerVisible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
        <TextInput placeholder="login"
          onChangeText={(text) => {
            this.setState({ login: text })
          }}
        />
        <TextInput placeholder="password"
          onChangeText={(text) => {
            this.setState({ password: text })
          }}
        />
        <Button disabled={!this.state.login || !this.state.password || this.state.spinnerVisible} title="Sign In" onPress={this.singIn.bind(this)}>
        </Button>
        {/*<ActivityIndicator
          style={{
            height: 80,
            opacity: this.state.spinnerVisible ? 1 : 0
          }}
          size="large"
        />*/}
        {/*<Text>{this.state.loginFailed}</Text>*/}
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3b5998',
    height: '100%'
  },
  webView: {
    height: 350,
  },
});

