import React from 'react';
import {
  View,
  Button
} from 'react-native';
import { connect } from 'react-redux';
var RNFetchBlob = require('react-native-fetch-blob').default;

class Home extends React.Component {
  loadTiles() {
    var path = RNFetchBlob.fs.dirs.DownloadDir;
    RNFetchBlob.fs.readFile(path + "/cos.txt", 'base64')
      .then((data) => {
        this.json = JSON.parse(Buffer.from(data, 'base64').toString());
        // this.json.features.forEach(function(peak) {
        //   console.info(peak.geometry.coordinates);
        // }, this);
      }, this)
  }

  setDataToStore(){
    
  }

  fetchTiles() {
    const requests = [
      "/api/peaks",
    ];
    requests.forEach(function (request) {
      RNFetchBlob
        // .config({
        //   path: RNFetchBlob.fs.dirs.DownloadDir + "/tiles/" + "jakis.json",
        //   fileCache: true,
        //   addAndroidDownloads: {
        //     // Show notification when response data transmitted
        //     notification: true,
        //     // Title of download notification
        //     title: 'Great ! Download Success ! :O ',
        //     // File description (not notification description)
        //     description: 'An image file.'
        //   }
        // })
        .fetch('GET', 'http://192.168.1.28:3000' + request)
        .progress({ interval: 10 }, (received, total) => {
          console.log('progress', received / total)
        })
        .then((res) => {
          RNFetchBlob.fs.writeFile(RNFetchBlob.fs.dirs.DownloadDir + "/cos.txt", res.data, 'base64')
            .then((info) => {
            })
          console.info("haaaaa");
        }, this)
        .catch((errorMessage, statusCode) => {
          console.info(errorMessage);
        })
    }, this);
  }

  render() {
    return (
      <View>
        <View style={{ alignItems: 'center', marginTop: 50 }}>
          <Button title="SignIn" color="#841584"
            onPress={() => { navigate('SignIn') }}>
          </Button>
        </View>
        <View style={{ alignItems: 'center', marginTop: 50 }}>
          <Button title="SignUp"
            onPress={() => { navigate('Map') }}>
          </Button>
        </View>
        <View style={{ alignItems: 'center', marginTop: 50 }}>
          <Button title="Download vector tiles" color="#841584"
            onPress={this.fetchTiles.bind(this)}>
          </Button>
          <View style={{ alignItems: 'center', marginTop: 50 }}>
            <Button title="Load vector tiles" color="#841584"
              onPress={this.loadTiles.bind(this)}>
            </Button>
          </View>
           <View style={{ alignItems: 'center', marginTop: 50 }}>
            <Button title="Load vector tiles2" color="#841584"
              onPress={this.setDataToStore.bind(this)}>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}


const mapStateToProps = (state) => {
  return { counter: state.counter };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onIncrement: () => dispatch({ type: 'SETDATA' }),
    onDecrement: () => dispatch({ type: 'DECREMENT' })
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
