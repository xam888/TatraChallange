var L = require('leaflet');
// require('../../node_modules/leaflet-basemaps/L.Control.Basemaps-min.js');
require('../../node_modules/leaflet.vectorgrid/dist/Leaflet.VectorGrid.bundled.js');
// var dijkstra = require('./geomUtils/dijkstra.js');
var paths = require('./paths.json');
// var peaks = require('./peaks.json');
// var getClosestPathAndJunction = require('./geomUtils/getClosestPathAndJunction.js');
var geojsonFeature = [], allPoints = [], currentLocation = [];

WebViewBridge.onMessage = function (message) {
    var message = JSON.parse(message);
    currentLocation.push({
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [message.coords.longitude, message.coords.latitude]
        }
    })
    L.geoJSON(currentLocation, {
        style: function (feature) {
            return {
                color: "orange",
                weight: 5
            }
        }
    }).addTo(map);
}

function onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.popupContent) {
        layer.bindPopup(feature.properties.popupContent);
    }
}

// var geojsonMarkerOptions = {
//     radius: 4,
//     fillColor: "#ff7800",
//     color: "#000",
//     weight: 1,
//     opacity: 1,
//     fillOpacity: 0.8
// };

// var junctionPoints = {};

paths.data.forEach(function (path) {
    path.geometry = JSON.parse(path.geometry);
    /////////////// dijkstra
    // var firstCoordinate = path.geometry.coordinates[0]
    // var secondCoordinate = path.geometry.coordinates[path.geometry.coordinates.length - 1];
    // var coordinatesLength = path.geometry.coordinates;
    // if (!junctionPoints[firstCoordinate.toString()]) {
    //     junctionPoints[firstCoordinate.toString()] = {
    //     }
    // }
    // junctionPoints[firstCoordinate.toString()][secondCoordinate.toString()] = path.real_length
    // if (!junctionPoints[secondCoordinate.toString()]) {
    //     junctionPoints[secondCoordinate.toString()] = {
    //     }
    // }
    // junctionPoints[secondCoordinate.toString()][firstCoordinate.toString()] = path.real_length
    /////////////// dijkstra
    path.geometry.coordinates.forEach(function (coordinate) {
        coordinate[2] = path.osm_id
        allPoints.push(coordinate);
    });
    geojsonFeature.push({
        type: "Feature",
        properties: {
            colour: path.colour
            // popupContent: path.name + "<br> czas tam: " + path.time_there + "<br> czas z powrotem: " + path.time_back
        },
        geometry: path.geometry
    })
}, this);


// peaks.data.forEach(function (peak) {
//     peak.geometry = JSON.parse(peak.geometry);
//     geojsonFeature.push({
//         type: "Feature",
//         properties: {
//             popupContent: peak.name + "<br> wysokość: " + peak.ele
//         },
//         geometry: peak.geometry
//     })
// }, this);


// var map = L.map('map')
// map.fitBounds([
//     [49.0250, 19.5502],
//     [49.3538, 20.3975]
// ]);
// L.DomUtil.TRANSITION_END = 'webkitTransitionEnd';
var map = L.map('map', {
    center: [49.1929, 19.9265],
    zoom: 10,
    zoomAnimation: false
})

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
// L.vectorGrid.protobuf("http://192.168.1.28:3000/api/vector-tiles/layername/{z}/{x}/{y}.pbf", {
//     rendererFactory: L.canvas.tile,
// }).addTo(map);


// map.whenReady(function(){
//     WebViewBridge.send("componentLoaded");
// })

// L.geoJSON(geojsonFeature, {
//     style: function (feature) {
//         return {
//             color: feature.properties.colour,
//             weight: 2
//         }
//     },
//     // onEachFeature: onEachFeature,
//     pointToLayer: function (feature, latlng) {
//         return L.circleMarker(latlng, geojsonMarkerOptions);
//     }
// }).addTo(map);
// var geoJsonDocument = {
//     type: 'FeatureCollection',
//     features: geojsonFeature
// };
// L.vectorGrid.slicer(geoJsonDocument, {
//     rendererFactory: L.canvas.tile,
// }).addTo(map);

// this.paths = paths;
// this.points = allPoints;
// this.getClosestPathAndJunction = getClosestPathAndJunction
// this.routingPoints = [];
// this.routingPaths = [];
// map.on('click', function (e) {
//     var closestFeatures = [];
//     var closestPathAndJunction = this.getClosestPathAndJunction(e, allPoints, paths)
//     var closestPath = closestPathAndJunction.closestPath;
//     var closestJunction = closestPathAndJunction.closestJunction;
//     this.routingPoints.push(closestJunction);
//     var newgraph = new dijkstra(junctionPoints);
//     if (this.routingPoints.length === 2) {
//         var shortestPathsJunctions = newgraph.findShortestPath(this.routingPoints[0][0] + "," + this.routingPoints[0][1], this.routingPoints[1][0] + "," + this.routingPoints[1][1]);
//         this.routingPaths = this.paths.data.filter(function (path) {
//             this.path = path;
//             return (shortestPathsJunctions.some(function (junction, index, array) {
//                 if (index !== array.length - 1) {
//                     var coor = this.path.geometry.coordinates;
//                     var lan = parseFloat(junction.split(",")[0]);
//                     var lon = parseFloat(junction.split(",")[1]);
//                     var lan2 = parseFloat(array[index + 1].split(",")[0]);
//                     var lon2 = parseFloat(array[index + 1].split(",")[1]);
//                     return (lan === coor[0][0] && lon === coor[0][1] &&
//                         lan2 === coor[coor.length - 1][0] && lon2 === coor[coor.length - 1][1]) || (lan2 === coor[0][0] && lon2 === coor[0][1] &&
//                             lan === coor[coor.length - 1][0] && lon === coor[coor.length - 1][1])
//                 }
//             }, this))
//         }, this)
//         this.routingPaths.forEach(function (routingPath) {
//             closestFeatures.push({
//                 type: "Feature",
//                 geometry: routingPath.geometry
//             })
//         });
//         // shortestPathsJunctions.forEach(function (point) {
//         //     closestFeatures.push({
//         //         type: "Feature",
//         //         geometry: {
//         //             type: "Point",
//         //             coordinates: [parseFloat(point.split(',')[0]), parseFloat(point.split(',')[1])]
//         //         }
//         //     })
//         // });
//         this.routingPoints.length = 0;
//     }
//     closestFeatures.push(
//         // {
//         //     type: "Feature",
//         //     properties: {
//         //         popupContent: closestPath.popupContent
//         //     },
//         //     geometry: closestPath.geometry
//         // },
//         {
//             type: "Feature",
//             properties: {
//             },
//             geometry: {
//                 type: "Point",
//                 coordinates: [closestJunction[0], closestJunction[1]]
//             }
//         });

//     var geojsonMarkerOptions = {
//         radius: 4,
//         fillColor: "#ff7800",
//         color: "#000",
//         weight: 1,
//         opacity: 1,
//         fillOpacity: 0.8
//     };
//     var geoJson = L.geoJSON(closestFeatures, {
//         style: function (feature) {
//             return {
//                 color: "orange",
//                 weight: 5
//             }
//         },
//         onEachFeature: onEachFeature
//     }).addTo(map);
//     // var layers = geoJson.getLayers();
//     // layers[0].openPopup()
// }, this);


// var basemaps = [
//     L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
//         attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
//     }),
//     L.tileLayer.wms('http://mapy.geoportal.gov.pl/wss/service/img/guest/ORTO/MapServer/WMSServer', {
//         layers: 'raster',
//         label: 'Ortofoto'
//     })
// ];

// map.addControl(L.control.basemaps({
//     basemaps: basemaps,
//     tileX: 0,  // tile X coordinate
//     tileY: 0,  // tile Y coordinate
//     tileZ: 1   // tile zoom level
// }));

