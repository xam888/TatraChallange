const L = require('leaflet');
var map = L.map('map', {
    center: [49.1929, 19.9265],
    zoom: 10,
    zoomAnimation: false
})

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);