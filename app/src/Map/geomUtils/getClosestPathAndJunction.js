var mathjs = require('mathjs');

function getClosestPathAndJunction(e, points, paths){
    var minDist, finalX, finalY, finalPath;
    var distances = [];
    var clickPoint = {
        x: e.latlng.lat,
        y: e.latlng.lng
    }
    this.points.forEach(function (point) {
        distances.push(mathjs.distance([clickPoint.x, clickPoint.y], [point[1], point[0]]));
        distances.push(point[1], point[0], point[2]);
    }, this);
    minDist = mathjs.min(distances);
    distances.some(function (distance, index, array) {
        finalX = array[index + 1];
        finalY = array[index + 2];
        finalPath = array[index + 3];
        return distance === minDist;
    });

    var closestPath;
    var closestJunction;
    paths.data.some(function(path){
        closestPath = {
            popupContent: path.name + "<br> czas tam: " + path.time_there + "<br> czas z powrotem: " + path.time_back,
            geometry: path.geometry,
            closestJunctions: [path.geometry.coordinates[0], path.geometry.coordinates[path.geometry.coordinates.length-1]]
        }
        return path.osm_id === finalPath
    });

    var distanceToJunction1 = mathjs.distance([clickPoint.x, clickPoint.y],
     [closestPath.closestJunctions[0][0], closestPath.closestJunctions[0][1]])

     var distanceToJunction2 = mathjs.distance([clickPoint.x, clickPoint.y],
     [closestPath.closestJunctions[1][0], closestPath.closestJunctions[1][1]])
     
     distanceToJunction1 > distanceToJunction2 ? closestJunction = closestPath.closestJunctions[1] : closestJunction = closestPath.closestJunctions[0]

    return {
        closestPath: closestPath,
        closestJunction: closestJunction
    }
}

module.exports = getClosestPathAndJunction;