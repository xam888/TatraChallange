package com.awesomeproject;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.github.alinz.reactnativewebviewbridge.WebViewBridgePackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.reactnativenavigation.NavigationApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;



public class MainApplication extends NavigationApplication implements ReactApplication {

  // private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
  //   @Override
  //   public boolean getUseDeveloperSupport() {
  //     return BuildConfig.DEBUG;
  //   }

  //   @Override
  //   protected List<ReactPackage> getPackages() {
  //     return Arrays.<ReactPackage>asList(
  //         new MainReactPackage(),
  //           new WebViewBridgePackage(),
  //           new RNFetchBlobPackage()
  //     );
  //   }
  // };

  // @Override
  // public ReactNativeHost getReactNativeHost() {
  //   return mReactNativeHost;
  // }

  // @Override
  // public void onCreate() {
  //   super.onCreate();
  //   SoLoader.init(this, /* native exopackage */ false);
  // }

    @Override
     public boolean isDebug() {
        //  Make sure you are using BuildConfig from your own application
         return BuildConfig.DEBUG;
     }

     @Override
     public List<ReactPackage> createAdditionalReactPackages() {
        //  Add the packages you require here.
        //  No need to add RnnPackage and MainReactPackage
          return Arrays.<ReactPackage>asList(
          new WebViewBridgePackage(),
           new RNFetchBlobPackage()
          );
        //  return null;
     }

}

// public class MainApplication extends NavigationApplication{
    

//    @Override
//      public boolean isDebug() {
//          // Make sure you are using BuildConfig from your own application
//          return BuildConfig.DEBUG;
//      }

//      @Override
//      public List<ReactPackage> createAdditionalReactPackages() {
//          // Add the packages you require here.
//          // No need to add RnnPackage and MainReactPackage
//           return Arrays.<ReactPackage>asList(
//           new WebViewBridgePackage(),
//            new RNFetchBlobPackage()
//           );
//         //  return null;
//      }
// }
